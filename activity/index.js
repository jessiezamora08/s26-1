const http = require ("http");

const port= 3000 

const server = http.createServer ((req,res) => {
    if (req.url == '/login') {
        res.writeHead (200, {'Content-Type': 'text/plain'})
        res.end ('Welcome to the login page.')
    }
    else  {
        res.writeHead (404,{'Content-Type' : 'text/plain'})
        res.end ('Page not found!');
    }

})

server.listen(port);


console.log(`server now accesible at localhost: ${port}`)


