/* ACTIVITY 

1. What directive is used by Node.js in loading the modules it needs?

♥ ANSWER:  'require' directive

2.  What Node.js module contains a method for server creation?

♦ ANSWER: http module

3.  What is the method of the http object responsible for creating a server using Node.js?

♣ ANSWER:  createServer()

4. What method of the response object allows us to set status codes and content types?

♠ ANSWER:  writehead()

5. Where will console.log() output its contents when run in Node.js?

♥ ANSWER: terminal 

6. What property of the request object contains the address's endpoint?

♦ ANSWER: .listen 

*/