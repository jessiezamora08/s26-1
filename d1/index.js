/* use the 'require' directive to load Node.js modules 
a "module" is a software component or part of a program that 
contains one or more routines  */

let http = require("http");

/*the http module can transfer data using the Hyper Text 
transfer protocol. It is a set of individual files that 
contain code to create a "component" thathelps establish 
data transfer between applications 

HTTP is a protocol that allows the fetching of resources such
as HTML documents 

Client - browser and server (nodeJS) communicate by 
exchanging individual messages. 

♠ The messages sent by the client, usually a web browser , 
are call "requests". 

♣ The messages sent by the server as an answer are called
"responses".  */

/* Using this modules's createServer() method, we can create 
an HTTP server that listens to requests on a specified address
/URL.port and gives responses back to the client  */

/* ♣ port is a virtual point where network connetions
start and end. Each port is associated with a specific 
process or service. 
http://localhost:4000
♠ the server will be assigned to port 4000 via the "listen 
(4000)" method where the server will listen to any requests 
that are sent to our server 

  */

http
  .createServer(function (request, response) {
    response.writeHead(200, { "Content-Type": "text/plain" });
    response.end("Hello World");
  })
  .listen(4000);

console.log("Server running at localhost:4000"); // you can see this in
// the terminal

/* 
♥ 200 - successful, OK
♦ we use the writehead() method to: set the status code for the response
♣ set the content-type of the response as a plain text message  
♠ value of content type can be image/jpeg or application/json
♥send the 
*/
