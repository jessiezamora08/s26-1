const http = require ("http");

// create a variable "port" to store the port number

const port = 4000

//create a variable "server" thats stores the output of the "createServer"
// method

const server = http.createServer((req, res) => {
    // acessing "greeting" route that returns a message of "Hello Again"
    // if statements to be used 
    // http://localhost:4000/greeting
    // request is an object that is sent via the client (browser)

    // .url - property refers to the url sa browser 

    if (req.url == '/greeting') {
        res.writeHead (200, {'Content-Type': 'text/plain'})
        res.end ('Hello Again')
    }
    else if(req.url == '/homepage') {
        res.writeHead (200,{'Content-Type' : 'text/plain'})
        res.end ('This is the homepage');
    }
    else {
        res.writeHead (404,{'Content-Type' : 'text/plain'})
        res.end ('Page not found!');
    }
})

server.listen(port);

//when server i
console.log(`server now accesible at localhost: ${port}`)
